package tw.idv.fy.widget.gamepoint.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.Locale;

import tw.idv.fy.widget.gamepoint.DraggableImageButtonAdapter;
import tw.idv.fy.widget.gamepoint.DraggableImageButtonDecoration;
import tw.idv.fy.widget.gamepoint.GamePoints;

public class RecyclerViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DraggableImageButtonDecoration(this));

        GridLayoutManager layoutManager = new GridLayoutManager(this, 10);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter adapter = new DraggableImageButtonAdapter(new GamePoints(5, 1, 2, 1, 1));
        recyclerView.setAdapter(adapter);

        findViewById(R.id.query).setOnClickListener(button -> {
            GamePoints nowGamePoints = ((DraggableImageButtonAdapter) adapter).queryNowGamePoints();
            Toast.makeText(button.getContext(), String.format(Locale.TAIWAN,
                    "國文 = %1$d\n數學 = %2$d\n自然 = %3$d\n社會 = %4$d\n英文 = %5$d",
                    nowGamePoints.queryGamePoint(GamePoints.LEVEL_CHINESE),
                    nowGamePoints.queryGamePoint(GamePoints.LEVEL_MATH),
                    nowGamePoints.queryGamePoint(GamePoints.LEVEL_NATURAL),
                    nowGamePoints.queryGamePoint(GamePoints.LEVEL_SOCIETY),
                    nowGamePoints.queryGamePoint(GamePoints.LEVEL_ENGLISH)
            ), Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
    }
}