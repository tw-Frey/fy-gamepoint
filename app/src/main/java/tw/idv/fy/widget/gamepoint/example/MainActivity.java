package tw.idv.fy.widget.gamepoint.example;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Outline;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewOutlineProvider;

import com.google.common.collect.FluentIterable;
import com.google.common.primitives.Ints;

import tw.idv.fy.widget.gamepoint.DragDropConstants;

import static android.view.DragEvent.ACTION_DRAG_ENTERED;
import static android.view.DragEvent.ACTION_DRAG_EXITED;
import static android.view.DragEvent.ACTION_DROP;
import static tw.idv.fy.widget.gamepoint.DragDropConstants.ACTION_DRAG_List;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View tv1 = findViewById(android.R.id.text1);
        tv1.setClipToOutline(true);
        tv1.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        tv1.setOnLongClickListener(v ->
                ViewCompat.startDragAndDrop(v, null, new MyDragShadowBuilder(v), null, 0)
        );
        tv1.setOnDragListener((v, event) -> {
            Log.v("Faty", ACTION_DRAG_List.get(event.getAction()) + " = " + event.toString());
            return true;
        });

        View tv2 = findViewById(android.R.id.text2);
        tv2.setClipToOutline(true);
        tv2.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                outline.setOval(0, 0, view.getWidth(), view.getHeight());
            }
        });
        tv2.setOnTouchListener((v, event) ->
                event.getAction() == MotionEvent.ACTION_DOWN
                        && ViewCompat.startDragAndDrop(v, null, new MyDragShadowBuilder(v), null, 0)
        );
        tv2.setOnDragListener((v, event) -> {
            Log.d("Faty", ACTION_DRAG_List.get(event.getAction()) + " = " + event.toString());
            return true;
        });

        findViewById(R.id.button).setOnDragListener(new View.OnDragListener() {
            Runnable r;
            @Override
            public boolean onDrag(final View v, DragEvent event) {
                int action = event.getAction();
                if (action == ACTION_DRAG_EXITED) {
                    if (r != null) v.removeCallbacks(r);
                    r = null;
                } else if (action == ACTION_DRAG_ENTERED && r == null) {
                    r = new Runnable() {
                        @Override
                        public void run() {
                            /*
                                參考:
                                https://stackoverflow.com/questions/27225014/how-to-trigger-ripple-effect-on-android-lollipop-in-specific-location-within-th#38242102
                                https://stackoverflow.com/questions/25407291/android-l-play-ripple-effect-on-view/25415471#25415471
                             */
                            v.setPressed(true);
                            v.setPressed(false);
                            v.postOnAnimationDelayed(this, 500);
                        }
                    };
                    v.postOnAnimation(r);
                }
                return true;
            }
        });
    }

    private static class MyDragShadowBuilder extends View.DragShadowBuilder {

        MyDragShadowBuilder(View view) {
            super(view);
        }

        @Override
        public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {
            super.onProvideShadowMetrics(outShadowSize, outShadowTouchPoint);
            outShadowSize.x *= 2;
            outShadowSize.y *= 2;
            outShadowTouchPoint.x *=2;
            outShadowTouchPoint.y *=2;
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            Drawable d = ResourcesCompat.getDrawable(
                    getView().getResources(),
                    R.mipmap.ic_launcher_round,
                    null
            );
            Rect rect = new Rect();
            getView().getDrawingRect(rect);
            assert d != null;
            rect.right += rect.width();
            rect.bottom += rect.height();
            d.setBounds(rect);
            d.draw(canvas);
        }
    }

    public static class MyTextView extends android.support.v7.widget.AppCompatTextView {

        public MyTextView(Context context) {
            super(context);
        }

        public MyTextView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }

        @Override
        public boolean onDragEvent(DragEvent event) {
            Log.w("Faty", ACTION_DRAG_List.get(event.getAction()) + " = " + event.toString());
            return event.getAction() < ACTION_DROP;//super.onDragEvent(event);
        }

        @SuppressWarnings("Guava")
        @Override
        protected int[] onCreateDrawableState(int extraSpace) {
            int[] states = super.onCreateDrawableState(extraSpace);
            android.util.Log.i("Faty", extraSpace + " = " +
                    FluentIterable
                            .from(Ints.asList(states))
                            .transform(DragDropConstants.STATE_MAP::get)
                            .toString()
            );
            return states;
        }
    }
}