package tw.idv.fy.widget.gamepoint;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.View;
import android.widget.Checkable;
import android.widget.ImageButton;

import java.util.Objects;

import static android.view.DragEvent.*;
import static tw.idv.fy.widget.gamepoint.DragDropHelper.*;
import static tw.idv.fy.widget.gamepoint.R.attr.tw_idv_fy_widget_gamepoint_DraggableImageButtonStyle;
import static tw.idv.fy.widget.gamepoint.R.style.tw_idv_fy_widget_gamepoint_DraggableImageButtonTheme;
import static tw.idv.fy.widget.gamepoint.R.styleable.tw_idv_fy_widget_gamepoint_DraggableImageButton;
import static tw.idv.fy.widget.gamepoint.R.styleable.tw_idv_fy_widget_gamepoint_DraggableImageButton_checked;
import static tw.idv.fy.widget.gamepoint.R.styleable.tw_idv_fy_widget_gamepoint_DraggableImageButton_hotspotRadius;

@SuppressLint({"AppCompatCustomView", "CustomViewStyleable"})
public class DraggableImageButton extends ImageButton implements View.OnLongClickListener, Checkable {

    private static final int[] STATE_CHECKED = {android.R.attr.state_checked};

    private final OnFinishDragDropListener DefaultOnFinishDragDropListener = this::toggle;
    /*package*/ final DragDropFlag mDragDropFlag = new DragDropFlag();

    private OnFinishDragDropListener mOnFinishDragDropListener;
    private Runnable flashHotSpotRunnable;

    private int mHotSpotRadius;
    private boolean mChecked;

    public DraggableImageButton(Context context) {
        this(context, null);
    }

    public DraggableImageButton(Context context, AttributeSet attrs) {
        this(context, attrs, tw_idv_fy_widget_gamepoint_DraggableImageButtonStyle);
    }

    public DraggableImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, tw_idv_fy_widget_gamepoint_DraggableImageButtonTheme);
    }

    public DraggableImageButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        boolean checked;
        int radius;
        TypedArray a = context.obtainStyledAttributes(
                attrs, tw_idv_fy_widget_gamepoint_DraggableImageButton, defStyleAttr, defStyleRes
        );
        checked = a.getBoolean(tw_idv_fy_widget_gamepoint_DraggableImageButton_checked, false);
        radius = a.getDimensionPixelSize(tw_idv_fy_widget_gamepoint_DraggableImageButton_hotspotRadius, -1);
        a.recycle();
        setChecked(checked);
        setHotSpotRadius(radius);
        setImageLevel(getTag());
        setOnLongClickListener(this);
        /*
            設定 OnFinishDragDropListener; 預設: DefaultOnFinishDragDropListener
         */
        setOnFinishDragDropListener(DefaultOnFinishDragDropListener);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        /*
            ripple drawable radius support from API 23
            所以在 API 23 之前要手動擴充 hot spot 的範圍
         */
        Drawable d = getBackground();
        if (d != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            d.setHotspotBounds(
                    w / 2 - mHotSpotRadius,
                    h / 2 - mHotSpotRadius,
                    w / 2 + mHotSpotRadius,
                    h / 2 + mHotSpotRadius
            );
        }
    }

    @Override
    public void setClickable(boolean clickable) {
        if (clickable) throw new UnsupportedOperationException();
        super.setClickable(false);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean onLongClick(View v) {
        if (mChecked && StartDragAndDrop(this, null, 0)) {
            setDraggingFlag(this, true);
            toggle();
            return true;
        }
        return false;
    }

    @Override
    public boolean onDragEvent(DragEvent event) {
        /*
            檢查是否同源
         */
        if (!CheckDragEventLike(event, this)) return false;
        /*
            0: UNDEFINED
            1: ACTION_DRAG_STARTED
            2: ACTION_DRAG_LOCATION
            3: ACTION_DROP
            4: ACTION_DRAG_ENDED
            5: ACTION_DRAG_ENTERED
            6: ACTION_DRAG_EXITED
         */
        int action = event.getAction();
        /*
            拖曳過程
         */
        if (action == ACTION_DRAG_ENTERED || action == ACTION_DRAG_STARTED && CheckDragEventSelf(event, this)) {
            /*
                開啟閃爍 (進入拖曳)
             */
            if (flashHotSpotRunnable == null) {
                flashHotSpotRunnable = new Runnable() {
                    @Override
                    public void run() {
                        flashHotSpot();
                        postOnAnimationDelayed(this, 480);
                    }
                };
                postOnAnimation(flashHotSpotRunnable);
            }
        } else if (action == ACTION_DRAG_EXITED || action == ACTION_DRAG_ENDED) {
            /*
                關閉閃爍 (退出拖曳)
             */
            if (flashHotSpotRunnable != null) {
                removeCallbacks(flashHotSpotRunnable);
                flashHotSpotRunnable = null;
                /*
                    ASUS 有時會留殘影, 所以強制再閃一次
                 */
                flashHotSpot();
            }
        }
        /*
            拖曳結束時
         */
        if (action == ACTION_DROP) {
            /*
                Drop 成功
             */
            SuccessDropped(event);
            if (mOnFinishDragDropListener instanceof QueryOnFinishDragDropListener) {
                ((QueryOnFinishDragDropListener) mOnFinishDragDropListener).setFunction(
                        ((View) event.getLocalState())::getTag, this::getTag
                );
            }
            postOnAnimation(mOnFinishDragDropListener::invoke);
        } else if (action == ACTION_DRAG_ENDED && CheckDragEventSelf(event, this) && !CheckDragDropHandleResult(event)) {
            /*
                Drop 失敗
             */
            FailureDropped(event);
            postOnAnimation(DefaultOnFinishDragDropListener::invoke);
        }
        return isDroppable();
    }

    @Override
    public int[] onCreateDrawableState(int extraSpace) {
        int[] states = super.onCreateDrawableState(extraSpace + 1);
        if (mChecked) {
            states = mergeDrawableStates(states, STATE_CHECKED);
        }
        return states;
    }

    @Override
    public void setChecked(boolean checked) {
        mChecked = checked;
        refreshDrawableState();
    }

    @Override
    public boolean isChecked() {
        return mChecked;
    }

    @Override
    public void toggle() {
        setChecked(!mChecked);
    }

    private boolean isDroppable() {
        return !mChecked;
    }

    private void setHotSpotRadius(int radius) {
        mHotSpotRadius = radius;
        Drawable d = getBackground();
        if (d instanceof RippleDrawable && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ((RippleDrawable) d).setRadius(mHotSpotRadius * 3 / 2);
        }
    }

    private void flashHotSpot() {
        /*
            參考:
            https://stackoverflow.com/questions/27225014/how-to-trigger-ripple-effect-on-android-lollipop-in-specific-location-within-th#38242102
            https://stackoverflow.com/questions/25407291/android-l-play-ripple-effect-on-view/25415471#25415471
         */
        setPressed(true);
        setPressed(false);
    }

    public void setImageLevel(Object tag) {
        setImageLevel(Integer.parseInt(Objects.toString(tag, "0")));
        if (getTag() == null) setTag(tag);
    }

/* ********************************
    Dropped 後 callback
 *********************************/

    public void setOnFinishDragDropListener(OnFinishDragDropListener listener) {
        mOnFinishDragDropListener = listener == null ? DefaultOnFinishDragDropListener : listener;
    }

    public class SimpleOnFinishDragDropListener implements OnFinishDragDropListener {
        public void invoke() {
            DefaultOnFinishDragDropListener.invoke();
        }
    }

    public interface OnFinishDragDropListener {
        void invoke();
    }

    /*package*/ class QueryOnFinishDragDropListener extends SimpleOnFinishDragDropListener {

        private Function mSrcFunction;
        private Function mDstFunction;

        private void setFunction(Function srcFunction, Function dstFunction) {
            mSrcFunction = srcFunction;
            mDstFunction = dstFunction;
        }

        @SuppressWarnings("unused")
        /*package*/ final Object srcQuery(@IdRes int id) {
            return Function.Apply(mSrcFunction, id);
        }

        @SuppressWarnings("unused")
        /*package*/ final Object dstQuery(@IdRes int id) {
            return Function.Apply(mDstFunction, id);
        }
    }

    private interface Function {

        Object apply(@IdRes int id);

        static Object Apply(Function mFunction, @IdRes int id) {
            if (mFunction == null) return null;
            return mFunction.apply(id);
        }
    }

}
