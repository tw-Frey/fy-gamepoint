package tw.idv.fy.widget.gamepoint;

import android.support.annotation.IntDef;
import android.support.annotation.IntRange;

import com.google.common.base.Verify;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class GamePoints {

    /*package*/ static final int MAX_GAMEPOINT = 10;
    /*package*/ static final int SUM_GAMEPOINT = 50;

    public static final int LEVEL_CHINESE = 0;
    public static final int LEVEL_MATH    = 1;
    public static final int LEVEL_NATURAL = 2;
    public static final int LEVEL_SOCIETY = 3;
    public static final int LEVEL_ENGLISH = 4;

    @Retention(SOURCE)
    @IntDef({LEVEL_CHINESE, LEVEL_MATH, LEVEL_NATURAL, LEVEL_SOCIETY, LEVEL_ENGLISH})
    private @interface LevelMode {}

    private final int[] points_array;

    /**
     * 每項不超過 {@link #MAX_GAMEPOINT}<br/>
     * 總和不超過 {@link #MAX_GAMEPOINT}<br/>
     * @param uey_chinese 國文能量: 0, 1, ... , 10 = {@link #MAX_GAMEPOINT}
     * @param uey_math    數學能量: 0, 1, ... , 10 = {@link #MAX_GAMEPOINT}
     * @param uey_natural 自然能量: 0, 1, ... , 10 = {@link #MAX_GAMEPOINT}
     * @param uey_society 社會能量: 0, 1, ... , 10 = {@link #MAX_GAMEPOINT}
     * @param uey_english 英語能量: 0, 1, ... , 10 = {@link #MAX_GAMEPOINT}
     */
    public GamePoints(@IntRange(from = 0, to = MAX_GAMEPOINT) int uey_chinese,
                      @IntRange(from = 0, to = MAX_GAMEPOINT) int uey_math,
                      @IntRange(from = 0, to = MAX_GAMEPOINT) int uey_natural,
                      @IntRange(from = 0, to = MAX_GAMEPOINT) int uey_society,
                      @IntRange(from = 0, to = MAX_GAMEPOINT) int uey_english) {
        VerifyGamePoint(
                uey_chinese + uey_math + uey_natural + uey_society + uey_english == MAX_GAMEPOINT
        );
        points_array = new int[]{uey_chinese, uey_math, uey_natural, uey_society, uey_english};
    }

    /**
     * clone
     */
    /*package*/ GamePoints(GamePoints gamePoints) {
        this(
                gamePoints.points_array[LEVEL_CHINESE],
                gamePoints.points_array[LEVEL_MATH],
                gamePoints.points_array[LEVEL_NATURAL],
                gamePoints.points_array[LEVEL_SOCIETY],
                gamePoints.points_array[LEVEL_ENGLISH]
        );
    }

    /**
     * 空數值(初始化用)
     */
    /*package*/ GamePoints() {
        points_array = new int[5];
    }

    /**
     *  檢查該 oneDimenIndex 是否有值<br/>
     *  這裡的 oneDimenIndex 是 一維<br/>
     *  即 oneDimenIndex = MAX_GAMEPOINT * row + column<br/>
     *  PS:<br/>
     *     row = 科目索引 = 0({@link #LEVEL_CHINESE}), 1({@link #LEVEL_MATH}), 2({@link #LEVEL_NATURAL}), 3({@link #LEVEL_SOCIETY}), 4({@link #LEVEL_ENGLISH}) <br/>
     *  column = 該科能量減一 = 0, 1, ... , 9  < {@link #MAX_GAMEPOINT} <br/>
     *  @param oneDimenIndex ViewAdapter 回報的 position (一維) (不大於{@link #SUM_GAMEPOINT})
     */
    /*package*/ boolean checkGamePoint(@IntRange(from = 0, to = SUM_GAMEPOINT - 1) int oneDimenIndex) {
        int POINT = points_array[PosToLevel(oneDimenIndex)];
        return 0 < POINT && (oneDimenIndex % MAX_GAMEPOINT) < POINT;
    }

    /**
     *  更新能量值組
     */
    /*package*/ void updateGamePoints(GamePoints gamePoints) {
        points_array[LEVEL_CHINESE] = gamePoints.points_array[LEVEL_CHINESE];
        points_array[LEVEL_MATH]    = gamePoints.points_array[LEVEL_MATH];
        points_array[LEVEL_NATURAL] = gamePoints.points_array[LEVEL_NATURAL];
        points_array[LEVEL_SOCIETY] = gamePoints.points_array[LEVEL_SOCIETY];
        points_array[LEVEL_ENGLISH] = gamePoints.points_array[LEVEL_ENGLISH];
    }

    /**
     * 查詢該科能量
     * @param level 科目索引
     * @return 0(無), 1, ..., 10 = {@link #MAX_GAMEPOINT}
     */
    @IntRange(from = 0, to = MAX_GAMEPOINT)
    public int queryGamePoint(@LevelMode int level) {
        return points_array[level];
    }

    /*package*/ void swapOneGamePoint(@LevelMode int srcLevel, @LevelMode int dstLevel) {
        points_array[srcLevel] -= 1;
        points_array[dstLevel] += 1;
        VerifyGamePoint(
                0 <= points_array[srcLevel] && points_array[srcLevel] < MAX_GAMEPOINT
                &&
                0 < points_array[dstLevel] && points_array[dstLevel] <= MAX_GAMEPOINT
        );
    }

    /**
     * 換算 Position 對應的 科目索引
     * @param position ViewAdapter 回報的 position (一維) (不大於{@link #SUM_GAMEPOINT})
     * @return 科目索引 0({@link #LEVEL_CHINESE}), 1({@link #LEVEL_MATH}), 2({@link #LEVEL_NATURAL}), 3({@link #LEVEL_SOCIETY}), 4({@link #LEVEL_ENGLISH})
     */
    @LevelMode
    /*package*/ static int PosToLevel(@IntRange(from = 0, to = SUM_GAMEPOINT - 1) int position) {
        return position / MAX_GAMEPOINT;
    }

    private static void VerifyGamePoint(boolean expression) {
        Verify.verify(expression, "能量分配有誤");
    }
}
