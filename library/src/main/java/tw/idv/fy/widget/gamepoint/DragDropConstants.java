package tw.idv.fy.widget.gamepoint;

import com.google.common.collect.Maps;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class DragDropConstants {

    public static final List<String> ACTION_DRAG_List = Collections.unmodifiableList(Arrays.asList(
            "UNDEFINED",
            "ACTION_DRAG_STARTED",
            "ACTION_DRAG_LOCATION",
            "ACTION_DROP",
            "ACTION_DRAG_ENDED",
            "ACTION_DRAG_ENTERED",
            "ACTION_DRAG_EXITED"
    ));

    public static final Map<Integer, String> STATE_MAP;
    static {
        Map<Integer, String> map = Maps.newHashMap();
        map.put(0, "undefined");
        map.put(android.R.attr.state_above_anchor, "state_above_anchor");
        map.put(android.R.attr.state_accelerated, "state_accelerated");
        map.put(android.R.attr.state_activated, "state_activated");
        map.put(android.R.attr.state_active, "state_active");
        map.put(android.R.attr.state_checkable, "state_checkable");
        map.put(android.R.attr.state_checked, "state_checked");
        map.put(android.R.attr.state_drag_can_accept, "state_drag_can_accept");
        map.put(android.R.attr.state_drag_hovered, "state_drag_hovered");
        map.put(android.R.attr.state_empty, "state_empty");
        map.put(android.R.attr.state_enabled, "state_enabled");
        map.put(android.R.attr.state_expanded, "state_expanded");
        map.put(android.R.attr.state_first, "state_first");
        map.put(android.R.attr.state_focused, "state_focused");
        map.put(android.R.attr.state_hovered, "state_hovered");
        map.put(android.R.attr.state_last, "state_last");
        map.put(android.R.attr.state_long_pressable, "state_long_pressable");
        map.put(android.R.attr.state_middle, "state_middle");
        map.put(android.R.attr.state_multiline, "state_multiline");
        map.put(android.R.attr.state_pressed, "state_pressed");
        map.put(android.R.attr.state_selected, "state_selected");
        map.put(android.R.attr.state_single, "state_single");
        map.put(android.R.attr.state_window_focused, "state_window_focused");
        STATE_MAP = Collections.unmodifiableMap(map);
    }

}
