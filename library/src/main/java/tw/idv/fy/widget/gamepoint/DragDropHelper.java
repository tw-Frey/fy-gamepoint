package tw.idv.fy.widget.gamepoint;

import android.content.ClipData;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.view.DragEvent;
import android.view.View;

import com.google.common.base.Optional;

import java.lang.ref.WeakReference;
import java.util.Objects;

/*package*/ class DragDropHelper {

    /*package*/ static class DragDropFlag {
        private boolean isDragging = false;
    }

    /**
     * 設定目標 mDraggingFlag
     */
    /*package*/ static void setDraggingFlag(Object src, boolean flag) {
        if (src instanceof DraggableImageButton) {
            ((DraggableImageButton) src).mDragDropFlag.isDragging = flag;
        }
    }

    /**
     * 成功完成 Drop: 關閉來源 mDraggingFlag
     */
    /*package*/ static void SuccessDropped(DragEvent event) {
        setDraggingFlag(event.getLocalState(), false);
    }

    /**
     * 失敗處理 Drop: 關閉來源 mDraggingFlag
     */
    /*package*/ static void FailureDropped(DragEvent event) {
        setDraggingFlag(event.getLocalState(), false);
    }

    /**
     *  檢查 DragDrop 是否被同類處理: 關閉來源 mDraggingFlag
     */
    /*package*/ static boolean CheckDragDropHandleResult(DragEvent event) {
        Object srcObj = event.getLocalState();
        return event.getResult()
            && srcObj instanceof DraggableImageButton
            && !((DraggableImageButton) srcObj).mDragDropFlag.isDragging;
    }

    /**
     *  檢查 DragEvent 是否源自本身
     */
    /*package*/ static boolean CheckDragEventSelf(DragEvent event, @NonNull Object self) {
        return Objects.equals(event.getLocalState(), self);
    }

    /**
     *  檢查 DragEvent 是否源自同類
     */
    /*package*/ static boolean CheckDragEventLike(DragEvent event, @NonNull Object self) {
        Object srcObj = event.getLocalState();
        return srcObj != null && Objects.equals(srcObj.getClass(), self.getClass());
    }

    /**
     *  客製化 startDragAndDrop (for DraggableImageButton)
     */
    @SuppressWarnings({"UnusedReturnValue", "SameParameterValue"})
    /*package*/ static boolean StartDragAndDrop(@NonNull DraggableImageButton v, ClipData data, int flags) {
        return ViewCompat.startDragAndDrop(v, data, new DragShadowBuilder(v), v, flags);
    }

    /**
     *  客製化 DragShadowBuilder (for DraggableImageButton)
     */
    @SuppressWarnings({"Guava", "OptionalUsedAsFieldOrParameterType"})
    private static class DragShadowBuilder extends View.DragShadowBuilder implements IDraw {

        private final transient Optional<IDraw> drawOptional;
        private static final int SCALE = 3;

        DragShadowBuilder(@NonNull DraggableImageButton view) {
            super(view);
            drawOptional = Optional.of(IDraw.newInstance(
                    new WeakReference<>(view.getDrawable())
            ));
        }

        @Override
        public void onProvideShadowMetrics(Point outShadowSize, Point outShadowTouchPoint) {
            super.onProvideShadowMetrics(outShadowSize, outShadowTouchPoint);
            outShadowSize.x *= SCALE;
            outShadowSize.y *= SCALE;
            outShadowTouchPoint.x *= SCALE;
            outShadowTouchPoint.y *= SCALE;
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
            canvas.scale(SCALE, SCALE);
            drawOptional.or(this).draw(canvas);
        }

        @Override
        public void draw(Canvas canvas) {
            super.onDrawShadow(canvas);
        }
    }

    private interface IDraw {
        void draw(Canvas canvas);
        static IDraw newInstance(final WeakReference<Drawable> d) {
            if (d == null || d.get() == null) return null;
            return d.get()::draw;
        }
    }
}
