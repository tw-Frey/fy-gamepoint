package tw.idv.fy.widget.gamepoint;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DraggableImageButtonDecoration extends RecyclerView.ItemDecoration {

    private final int mOffsetX;
    private final int mOffsetY;

    public DraggableImageButtonDecoration(Context context) {
        Resources res = context.getResources();
        mOffsetX = res.getDimensionPixelSize(R.dimen.tw_idv_fy_widget_gamepoint_offset_x);
        mOffsetY = res.getDimensionPixelSize(R.dimen.tw_idv_fy_widget_gamepoint_offset_y);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.set(mOffsetX, mOffsetY, mOffsetX, mOffsetY);
    }
}
