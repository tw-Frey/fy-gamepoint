package tw.idv.fy.widget.gamepoint;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import static android.support.v7.widget.RecyclerView.ViewHolder;
import static tw.idv.fy.widget.gamepoint.DraggableImageButtonAdapter.viewHolderId;
import static tw.idv.fy.widget.gamepoint.GamePoints.MAX_GAMEPOINT;
import static tw.idv.fy.widget.gamepoint.GamePoints.SUM_GAMEPOINT;
import static tw.idv.fy.widget.gamepoint.R.id.tw_idv_fy_widget_gamepoint_DraggableImageButton_viewHolder;

public class DraggableImageButtonAdapter extends RecyclerView.Adapter<DraggableImageButtonVHolder> {

    @IdRes
    /*package*/ static final int viewHolderId = tw_idv_fy_widget_gamepoint_DraggableImageButton_viewHolder;

    private final GamePoints mGamePoints;

    @SuppressWarnings("unused")
    public DraggableImageButtonAdapter(GamePoints gamePoints) {
        super();
        mGamePoints = new GamePoints(gamePoints);
    }

    /**
     * 空數值(初始化用)
     */
    public DraggableImageButtonAdapter() {
        super();
        mGamePoints = new GamePoints();
    }

    /**
     *  更新能量值組
     */
    public void updateGamePoints(GamePoints gamePoints) {
        mGamePoints.updateGamePoints(gamePoints);
    }

    /**
     *  clone 能量值組
     */
    public GamePoints queryNowGamePoints() {
        return new GamePoints(mGamePoints);
    }

    @NonNull
    @Override
    public DraggableImageButtonVHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DraggableImageButton v = new DraggableImageButtonWrapper(parent.getContext());
        v.setLayoutParams(LP.Gain(parent.getContext()));
        v.setOnFinishDragDropListener(v.new QueryOnFinishDragDropListener() {
            @Override
            public void invoke() {
                super.invoke();
                try {
                    ViewHolder srcVHolder = (ViewHolder) srcQuery(viewHolderId);
                    ViewHolder dstVHolder = (ViewHolder) dstQuery(viewHolderId);
                    ItemMoving(srcVHolder, dstVHolder);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return new DraggableImageButtonVHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull DraggableImageButtonVHolder viewHolder, int position) {
        DraggableImageButton v = (DraggableImageButton) viewHolder.itemView;
        v.setChecked(mGamePoints.checkGamePoint(position));
        v.setImageLevel(GamePoints.PosToLevel(position));
    }

    @Override
    public int getItemCount() {
        return SUM_GAMEPOINT;
    }

    private void ItemMoving(ViewHolder srcVHolder, ViewHolder dstVHolder) {
        int srcLayoutPosition = srcVHolder.getLayoutPosition();
        int dstLayoutPosition = dstVHolder.getLayoutPosition();
        /*
            step 0: 如果 src layout pos == dst layout pos, 則不作處理
         */
        if (srcLayoutPosition == dstLayoutPosition) return;
        /*
            step 1: 根據 src layout pos, dst layout pos 更新 data
         */
        int srcLevel = GamePoints.PosToLevel(srcLayoutPosition);
        int dstLevel = GamePoints.PosToLevel(dstLayoutPosition);
        mGamePoints.swapOneGamePoint(srcLevel, dstLevel);
        /*
            step 2: src item (此時 src item 應為空) 移到 同 Level 第一個空位上
         */
        int srcNewLevelGamePoint = mGamePoints.queryGamePoint(srcLevel);
        int srcNewLayoutPosition = MAX_GAMEPOINT * srcLevel + srcNewLevelGamePoint;
        notifyItemMoved(srcLayoutPosition, srcNewLayoutPosition);
        /*
            step 3: dst item (此時 dst item 應為實) 移到 同 Level 最大數值位置
         */
        if (srcNewLayoutPosition == dstLayoutPosition) {
            // 當 step 2 把 dst item 插入 src pos 空位, 則 不用做 step 3
            return;
        }
        int dstNewLevelGamePoint = mGamePoints.queryGamePoint(dstLevel);
        int dstNewLayoutPosition = MAX_GAMEPOINT * dstLevel + dstNewLevelGamePoint - 1;
        notifyItemMoved(dstLayoutPosition, dstNewLayoutPosition);
    }

    private static class LP {
        private static ViewGroup.LayoutParams lp;
        private static ViewGroup.LayoutParams Gain(Context context) {
            if (lp == null) {
                Resources res = context.getResources();
                lp = new ViewGroup.LayoutParams(
                        res.getDimensionPixelSize(R.dimen.tw_idv_fy_widget_gamepoint_width),
                        res.getDimensionPixelSize(R.dimen.tw_idv_fy_widget_gamepoint_height)
                );
            }
            return new ViewGroup.LayoutParams(lp);
        }
    }
}

/*package*/ class DraggableImageButtonVHolder extends ViewHolder {
    /*package*/ DraggableImageButtonVHolder(DraggableImageButton v) {
        super(v);
        v.setTag(viewHolderId, this);
    }
}

/**
 * 用來區分是否來自同源<br/>
 * 參考<br/>
 * {@link DragDropHelper#CheckDragEventLike(android.view.DragEvent, java.lang.Object)}
 */
/*package*/ class DraggableImageButtonWrapper extends DraggableImageButton {
    /*package*/ DraggableImageButtonWrapper(Context context) {
        super(context);
    }
}
