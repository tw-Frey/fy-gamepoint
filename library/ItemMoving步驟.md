<h3>RecyclerView.ViewHolder/notifyItemMoved 步驟</h3>
<h4>notifyItemMoved(fromPosition, toPosition)</h4>
<p>只是將位置 fromPosition 上的 item 移到位置 toPosition 去, 空缺其後補上<br/>
(<i>並非互換位置</i>)</p>
<br/>
<p>方案一</p>
<b>[step 0]</b> 如果 <code>src layout pos == dst layout pos</code>, 則不作處理<br/>
<b>[step 1]</b> <code>src item</code> 移到 同 Level 的最後一個空位, 並該 Level <code>數量減1</code><br/>
<b>[step 2]</b> <code>dst item</code> 移到 同 Level 的第一個位置上, 並該 Level <code>數量加1</code><br/>
<br/>
<br/>
<p>方案二</p>
<b>[step 0]</b> 如果 <code>src layout pos == dst layout pos</code>, 則不作處理<br/>
<b>[step 1]</b> 根據 src layout pos, dst layout pos 更新 data<br/>
<b>[step 2]</b> 根據 新 data 呼叫 notifyDataSetChanged()<br/>
<br/>
<br/>
<p>方案三</p>
<b>[step 0]</b> 如果 <code>src layout pos == dst layout pos</code>, 則不作處理<br/>
<b>[step 1]</b> 根據 src layout pos, dst layout pos 更新 data<br/>
<b>[step 2]</b> <code>src item (此時 src item 應為空)</code> 移到 同 Level 第一個空位上<br/>
<b>[step 3]</b> <code>dst item (此時 dst item 應為實)</code> 移到 同 Level 最大數值位置<br/>
<pre>
PS: 當 step 2 把 dst item 插入 src pos 空位, 則 不用做 step 3
</pre>